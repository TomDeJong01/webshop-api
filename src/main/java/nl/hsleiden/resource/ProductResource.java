package nl.hsleiden.resource;

import com.fasterxml.jackson.annotation.JsonView;
import com.google.inject.Singleton;
import io.dropwizard.auth.Auth;
import java.util.Collection;
import java.util.List;
import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import nl.hsleiden.View;
import nl.hsleiden.model.Product;
import nl.hsleiden.model.User;
import nl.hsleiden.service.ProductService;

/**
 * Meer informatie over resources:
 *  https://jersey.java.net/documentation/latest/user-guide.html#jaxrs-resources
 *
 * @author Peter van Vliet
 */
@Singleton
@Path("/products")
@Produces(MediaType.APPLICATION_JSON)
public class ProductResource {
  private final ProductService service;

  @Inject
  public ProductResource(ProductService productService)
  {
    this.service = productService;
  }

  @GET
  @JsonView(View.Public.class)
  @Produces(MediaType.APPLICATION_JSON)
  public List<Product> retrieveAll() { return service.getAll(); }

  @POST
  @Consumes(MediaType.APPLICATION_JSON)
  @JsonView(View.Protected.class)
  @RolesAllowed("ADMIN")
  @Produces(MediaType.TEXT_HTML)
  public void create(@Valid Product product)
  {
    service.add(product);
  }


  @PUT
  @Path("/{id}")
  @Consumes(MediaType.APPLICATION_JSON)
  @JsonView(View.Protected.class)
  @RolesAllowed("ADMIN")
  public void update(@PathParam("id") int id, Product product) {
    service.update(product);
  }

  @DELETE
  @Path("/{id}")
  @RolesAllowed("ADMIN")
  public void delete(@PathParam("id") int id)
  {
    service.delete(id);
  }

}
