package nl.hsleiden.persistence;

import nl.hsleiden.model.Order;
import nl.hsleiden.model.OrderProduct;
import nl.hsleiden.model.Product;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class OrderDAO {

    private static final String LOGGER_TEXT = "Just a stack trace, nothing to worry about";

    public int createOrder(int userId) {
        Order order = new Order();
        order.setUser_id(userId);
        ResultSet rs = null;
        String sql = "INSERT into user_order (user_id, order_status) values (?, ?) returning order_id";
        Connection connection = DBConnection.createConnection();
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, userId);
            ps.setString(2, "pending");
            rs = ps.executeQuery();
            while(rs.next()) {
                order.setOrder_id(rs.getInt("order_id"));
            }
            DBConnection.shutdown(connection);
        } catch (SQLException e) {
            Logger.getRootLogger().info(LOGGER_TEXT, e);
        }
        Logger.getRootLogger().info("order added");
        return order.getOrder_id();
    }

    public void addOrderItems(OrderProduct[] shoppingCartItems) {
    }


    public List<Order> getAll(int user_id) {
        List<Order> ordersList = new ArrayList<>();
        Connection connection = null;
        try (Connection tempConnection = DBConnection.createConnection()) {
            connection = tempConnection;
            String sql = "SELECT * FROM user_order WHERE user_id = " + user_id;
            executeGetOrdersQuery(connection, sql, ordersList);
        }catch(SQLException e) {
            Logger.getRootLogger().info(LOGGER_TEXT, e);
        } finally {
            DBConnection.shutdown(connection);
        }
        return ordersList;
    }

    private void executeGetOrdersQuery(Connection connection, String sql, List<Order> ordersList) {
        ResultSet rs = null;
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            rs = ps.executeQuery();
            while (rs.next()) {
                Order order = new Order();
                getOrder(order, rs);
                ordersList.add(order);
            }
            rs.close();
        } catch (SQLException e) {
            Logger.getRootLogger().info(LOGGER_TEXT, e);
        } finally {
            if (rs != null) {
                closeResultSet(rs);
            }
        }
    }

    private void getOrder(Order order, ResultSet rs) throws SQLException {
        order.setOrder_id(rs.getInt("order_id"));
        order.setOrder_status(rs.getString("order_status"));
    }

    public Optional<Order> get() {
        return null;
    }

    public void update() {
    }

    public void delete() {
    }

    public void fillOrder(List<OrderProduct> orderProducts, int order_id) {
        String sqlOrderProduct = "INSERT into order_item (order_id, product_id, amount) values (?, ?, ?) returning order_id";
        try (Connection connection = DBConnection.createConnection()) {
            for (OrderProduct orderProduct : orderProducts) {
                executeSaveOrderProductQuery(connection, sqlOrderProduct, orderProduct, order_id);
            }
            DBConnection.shutdown(connection);
        }catch (SQLException e) {
            Logger.getRootLogger().info(LOGGER_TEXT, e);
        }
        Logger.getRootLogger().info("orderProducts added");
    }

    private void executeSaveOrderProductQuery(Connection connection, String sqlOrderProduct, OrderProduct orderProduct, int order_id) {
        try (PreparedStatement ps = connection.prepareStatement(sqlOrderProduct)) {
            System.out.println("add product: " + order_id + "  " +orderProduct.getProduct().getProductId());
            ps.setInt(1, order_id);
            ps.setInt(2, orderProduct.getProduct().getProductId());
            ps.setInt(3, orderProduct.getAmount());
            ps.executeQuery();
        }catch (SQLException e) {
            Logger.getRootLogger().info(LOGGER_TEXT, e);
        }
    }

    private void closeResultSet(ResultSet rs) {
        try {
            rs.close();
        } catch (SQLException e) {
            Logger.getRootLogger().info(LOGGER_TEXT, e);
        }
    }
}
