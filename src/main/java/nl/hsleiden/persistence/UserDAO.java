package nl.hsleiden.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.inject.Singleton;
import nl.hsleiden.model.User;
import org.apache.log4j.Logger;

/**
 *
 * @author Peter van Vliet
 */
@Singleton
public class UserDAO {
    private final List<User> users;
    private static final String LOGGER_TEXT = "Just a stack trace, nothing to worry about";

    public UserDAO() {
        users = new ArrayList<>();
    }

    public List<User> getAll() {
        System.out.println("getAllUsers");
        List<User> users = new ArrayList<>();
        Connection connection = null;
        try (Connection tempConnection = DBConnection.createConnection()) {
            connection = tempConnection;
            String sql = "SELECT * FROM webshop_user";
            getAllUsersQuery(connection, sql, users);
        } catch (SQLException e) {
            Logger.getRootLogger().info(LOGGER_TEXT, e);
        } finally {
            DBConnection.shutdown(connection);
        }
        return users;
    }

    private void getAllUsersQuery(Connection connection, String sql, List<User> users) {
        ResultSet rs = null;
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            rs = ps.executeQuery();
            while (rs.next()) {
                User user = new User();
                getUser(user, rs);
                users.add(user);
            } rs.close();
        } catch (SQLException e) {
            Logger.getRootLogger().info(LOGGER_TEXT, e);
        } finally {
            if (rs != null) {
                closeResultSet(rs);
            }
        }
    }

    private void getUser(User user, ResultSet rs) throws SQLException {
        user.setUserId(rs.getInt("id"));
        user.setUsername(rs.getString("username"));
        user.setEmailAddress(rs.getString("email"));
        user.setDomain(rs.getString("domain"));
        user.setPassword(rs.getString("password"));
        user.setZipcode(rs.getString("zipcode"));
        user.setStreet(rs.getString("street"));
        user.setHouse_nr(rs.getInt("house_nr"));
        user.setPlace(rs.getString("place"));
    }

    public User get(int id) {
        try {
            return users.get(id);
        }
        catch(IndexOutOfBoundsException exception) {
            return null;
        }
    }

    public User get(String username) {
        User user = new User();
        String sql = "SELECT * FROM webshop_user WHERE username = '" + username + "'";
        try (Connection connection = DBConnection.createConnection()) {
            executeGetUserQuery(connection, user, sql);
            DBConnection.shutdown(connection);
        } catch (SQLException e) {
            Logger.getRootLogger().info(LOGGER_TEXT, e);
        } finally {

        }
        return user;
    }

    private void executeGetUserQuery(Connection connection, User user, String sql) {
        ResultSet rs = null;
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            rs = ps.executeQuery();
            while (rs.next()) {
                getUser(user, rs);
            }
            rs.close();
        } catch (SQLException e) {
            Logger.getRootLogger().info(LOGGER_TEXT, e);
        } finally {
            if (rs != null) {
                closeResultSet(rs);
            }
        }
    }

    public void add(User user) {
        String sqlContact = "INSERT into webshop_user (username, password, email, zipcode, street, house_nr, place, domain) values (?, ?, ?, ?, ?, ?, ?, ?) returning id";
        try (Connection connection = DBConnection.createConnection()) {
            executeSaveQuery(connection, sqlContact, user);
            System.out.println("adding user in dao");
            DBConnection.shutdown(connection);
        } catch (SQLException e) {
            Logger.getRootLogger().info(LOGGER_TEXT, e);
        }
        Logger.getRootLogger().info("contact added");
    }

    private void executeSaveQuery(Connection connection, String sqlContact, User user) {
        ResultSet rs = null;
        try (PreparedStatement ps = connection.prepareStatement(sqlContact)) {
            ps.setString(1, user.getUsername());
            ps.setString(2, user.getPassword());
            ps.setString(3, user.getEmailAddress());
            ps.setString(4, user.getZipcode());
            ps.setString(5, user.getStreet());
            ps.setInt(6, user.getHouse_nr());
            ps.setString(7, user.getPlace());
            ps.setString(8, "GUEST");
            rs = ps.executeQuery();
            while (rs.next()) {
                user.setUserId(rs.getInt("id"));
            }
        } catch (SQLException e) {
            Logger.getRootLogger().info(LOGGER_TEXT, e);
        } finally {
            if (rs != null) {
                closeResultSet(rs);
            }
        }
    }

    public void update(int id, User user)
    {
        users.set(id, user);
    }

    public void delete(int id)
    {
        users.remove(id);
    }

    private void closeResultSet(ResultSet rs) {
        try {
            rs.close();
        } catch (SQLException e) {
            Logger.getRootLogger().info(LOGGER_TEXT, e);
        }
    }


//    public User get(String username) {
//
//        return null;
//    }
}
