package nl.hsleiden.persistence;

import nl.hsleiden.model.Product;
import org.apache.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ProductDAO {
  private static final String LOGGER_TEXT = "Just a stack trace, nothing to worry about";

  private void closeResultSet(ResultSet rs) {
    try {
      rs.close();
    } catch (SQLException e) {
      Logger.getRootLogger().info(LOGGER_TEXT, e);
    }
  }


  public List<Product> getAll() {
    List<Product> productList = new ArrayList<>();
    Connection connection = null;
    try (Connection tempConnection = DBConnection.createConnection()) {
      connection = tempConnection;
      String sql = "SELECT * FROM product";
      getAllProductsQuery(connection, sql, productList);
    } catch (SQLException e) {
      Logger.getRootLogger().info(LOGGER_TEXT, e);
    } finally {
      DBConnection.shutdown(connection);
    }
    return productList;
  }

  private void getAllProductsQuery(Connection connection, String sql, List<Product> products) {
    ResultSet rs = null;
    try (PreparedStatement ps = connection.prepareStatement(sql)) {
      rs = ps.executeQuery();
      while (rs.next()) {
        Product product = new Product();
        getProduct(product, rs);
        products.add(product);
      }
      rs.close();
    } catch (SQLException e) {
      Logger.getRootLogger().info(LOGGER_TEXT, e);
    } finally {
      if (rs != null) {
        closeResultSet(rs);
      }
    }
  }

  public void getProduct(Product product, ResultSet rs) throws SQLException {
    product.setProductId(rs.getInt("product_id"));
    product.setName(rs.getString("name"));
    product.setCategory(rs.getString("category"));
    product.setPrice(rs.getDouble("price"));
    product.setBrewery(rs.getString("brewery"));
    product.setImgUrl(rs.getString("imgurl"));
  }

  public void add(Product product) {
    String sql = "INSERT into product (name, category, price, brewery, imgurl) values (?, ?, ?, ?, ?) returning product_id";
    try (Connection connection = DBConnection.createConnection()) {
      executeSaveQuery(connection, sql, product);
      DBConnection.shutdown(connection);
    } catch (SQLException e) {
      Logger.getRootLogger().info(LOGGER_TEXT, e);
    }
    Logger.getRootLogger().info("contact added");
  }

  private void executeSaveQuery(Connection connection, String sqlContact, Product product) {
    ResultSet rs = null;
    try (PreparedStatement ps = connection.prepareStatement(sqlContact)) {
      ps.setString(1, product.getName());
      ps.setString(2, product.getCategory());
      ps.setDouble(3, product.getPrice());
      ps.setString(4, product.getBrewery());
      ps.setString(5, product.getImgUrl());
      rs = ps.executeQuery();
      while (rs.next()) {
        product.setProductId(rs.getInt("product_id"));
      }
    } catch (SQLException e) {
      Logger.getRootLogger().info(LOGGER_TEXT, e);
    } finally {
      if (rs != null) {
        closeResultSet(rs);
      }
    }
  }

  public void delete(int id) {
    Connection connection = DBConnection.createConnection();
    String sql = "DELETE FROM product WHERE product_id = ?";
    try (PreparedStatement ps = connection.prepareStatement(sql)) {
      ps.setInt(1, id);
      ps.execute();
      }catch (SQLException e) {
      Logger.getRootLogger().info(LOGGER_TEXT, e);
      }
    }

  public void update(Product product) {
    String sql = "UPDATE product SET name = ?, category = ?, price = ?, brewery = ?, imgurl = ? WHERE product_id = ?";
    Connection connection = DBConnection.createConnection();
    try (PreparedStatement ps = connection.prepareStatement(sql)) {
      ps.setString(1, product.getName());
      ps.setString(2, product.getCategory());
      ps.setDouble(3, product.getPrice());
      ps.setString(4, product.getBrewery());
      ps.setString(5, product.getImgUrl());
      ps.setInt(6, product.getProductId());
      ps.execute();
    } catch (SQLException e) {
      Logger.getRootLogger().info(LOGGER_TEXT, e);
    }
  }


  public Optional<Product> get(int id) {
    return Optional.empty();
  }
}
