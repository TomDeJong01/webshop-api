package nl.hsleiden.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import java.security.Principal;
import nl.hsleiden.View;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.security.auth.Subject;

/**
 * Meer informatie over validatie:
 *  http://hibernate.org/validator/
 * 
 * @author Peter van Vliet
 */
public class User implements Principal {

    @NotEmpty
    @Length(min = 3, max = 100)
    @JsonView(View.Public.class)
    private String username;

    @JsonView(View.Public.class)
    private int userId;
    
    @NotEmpty
    @Email
    @JsonView(View.Public.class)
    private String emailAddress;
    
    @NotEmpty
    @Length(min = 6)
    @JsonView(View.Protected.class)
    private String password;

    private String zipcode;
    private String street;
    private int house_nr;
    private String place;
    
    @JsonView(View.Private.class)
    private String domain;

    @JsonView(View.Private.class)
    private String[] roles;

    public void setUserId(int userId) { this.userId = userId; }
    public int getUserId() { return userId; }

    public String getUsername() { return username; }
    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getEmailAddress()
    {
        return emailAddress;
    }
    public void setEmailAddress(String emailAddress)
    {
        this.emailAddress = emailAddress;
    }

    public String getPassword()
    {
        return password;
    }
    public void setPassword(String password)
    {
        this.password = password;
    }

    public String getZipcode() {return zipcode;}
    public void setZipcode(String zipcode) {this.zipcode = zipcode;}

    public String getStreet() {return street;}
    public void setStreet(String street) {this.street = street;}

    public int getHouse_nr() {return house_nr;}
    public void setHouse_nr(int house_nr) { this.house_nr = house_nr;}

    public String getPlace() {return place;}
    public void setPlace(String place) {this.place = place;}

    public String getDomain() { return domain;}
    public void setDomain(String domain) {
        this.domain = domain;
        setRoles(new String[] {"GUEST"});
        if(domain.equals("ADMIN")) {
            setRoles(new String[] {"ADMIN", "GUEST"});
        }
    }
    public String[] getRoles() { return roles; }
    public void setRoles(String[] roles) { this.roles = roles; }


    public boolean equals(User user)
    {
        return emailAddress.equals(user.getEmailAddress());
    }

    @Override
    public String getName() {
        return username;
    }

    public boolean hasRole(String roleName) {
        if (this.roles != null) {
            for(String role : this.roles) {
                if(roleName.equals(role)) {
                    return true;
                }
            }
        }
        return false;
    }
}
