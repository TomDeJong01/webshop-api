package nl.hsleiden.service;

import nl.hsleiden.model.Order;
import nl.hsleiden.model.OrderProduct;
import nl.hsleiden.model.Product;
import nl.hsleiden.model.User;
import nl.hsleiden.persistence.OrderDAO;
import nl.hsleiden.persistence.ProductDAO;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.List;
import java.util.Optional;

@Singleton
public class OrderService extends BaseService<Product> {
    private final OrderDAO dao;
    private final UserService userService;

    @Inject
    public OrderService(OrderDAO dao, UserService userservice)
    {
        this.dao = dao;
        this.userService = userservice;
    }

    public List<Order> getAll(String username) {
        int user_id = userService.get(username).getUserId();
        return dao.getAll(user_id); }

    public Optional<Order> get() {
        return dao.get();
    }

    public void add(OrderProduct shoppingCartItems[]) {
//        int order_id = dao.createOrder(username);
        System.out.println();
        dao.addOrderItems(shoppingCartItems);}

    public void update() {
        dao.update();
    }

    public void delete() { dao.delete(); }

    public int createOrder(String username) {
        System.out.println("create order " + username);
        return dao.createOrder(userService.get(username).getUserId());
    }

    public void fillOrder(List<OrderProduct> orderProducts, int order_id) {
        System.out.println("fill order: " + order_id);
        dao.fillOrder(orderProducts, order_id);
    }
}

