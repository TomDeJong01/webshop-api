package nl.hsleiden.service;

import nl.hsleiden.model.Product;
import nl.hsleiden.persistence.ProductDAO;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Singleton
public class ProductService extends BaseService<Product> {
  private final ProductDAO dao;

  @Inject
  public ProductService(ProductDAO dao)
  {
    this.dao = dao;
  }

  public List<Product> getAll() { return dao.getAll(); }

  public Optional<Product> get(int id) {
    return dao.get(id);
  }

  public void add(Product product) { dao.add(product);}

  public void update(Product product) {
    dao.update(product);
  }

  public void delete(int id) { dao.delete(id); }
}

