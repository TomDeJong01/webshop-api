package nl.hsleiden.model;

import com.fasterxml.jackson.annotation.JsonView;
import nl.hsleiden.View;
import org.hibernate.validator.constraints.NotEmpty;

import java.text.DecimalFormat;

public class Product  {

  DecimalFormat df = new DecimalFormat("#.00");


  @JsonView(View.Public.class)
  private int productId;

  @NotEmpty
  @JsonView(View.Public.class)
  private String name;

  @NotEmpty
  @JsonView(View.Public.class)
  private String category;

  @JsonView(View.Public.class)
  private Double price;

  @JsonView(View.Public.class)
  private double alcoholpercentage;

  @JsonView(View.Public.class)
  private String fermentation;

  @JsonView(View.Public.class)
  private String brewery;

  @JsonView(View.Public.class)
  private String imgUrl;

  public int getProductId() {
    return productId;
  }

  public void setProductId(int productId) {
    this.productId = productId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getCategory() {
    return category;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  public Double getPrice() {
    price = Double.valueOf(df.format(price));
    return price;
  }

  public void setPrice(Double price) {
    this.price = price;
  }

  public double getAlcoholpercentage() {
    return alcoholpercentage;
  }

  public void setAlcoholpercentage(double alcoholpercentage) {
    this.alcoholpercentage = alcoholpercentage;
  }

  public String getFermentation() {
    return fermentation;
  }

  public void setFermentation(String fermentation) {
    this.fermentation = fermentation;
  }

  public String getBrewery() {
    return brewery;
  }

  public void setBrewery(String brewery) {
    this.brewery = brewery;
  }

  public String getImgUrl() {    return imgUrl; }

  public void setImgUrl(String imgUrl) { this.imgUrl = imgUrl;}
}
