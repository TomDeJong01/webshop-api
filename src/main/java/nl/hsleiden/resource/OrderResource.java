package nl.hsleiden.resource;

import com.fasterxml.jackson.annotation.JsonView;
import com.google.inject.Singleton;
import nl.hsleiden.View;
import nl.hsleiden.model.Order;
import nl.hsleiden.model.OrderProduct;
import nl.hsleiden.model.Product;
import nl.hsleiden.service.BaseService;
import nl.hsleiden.service.OrderService;
import org.glassfish.jersey.model.internal.RankedComparator;

import javax.annotation.security.RolesAllowed;
import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Singleton
@Path("/order")
@Produces(MediaType.APPLICATION_JSON)
public class OrderResource {
    private final OrderService service;

    @Inject
    public OrderResource(OrderService orderService)
    {
        this.service = orderService;
    }

//    @GET
//    @JsonView(View.Public.class)
//    @Produces(MediaType.APPLICATION_JSON)
//    public List<Order> retrieveAll() { return service.getAll(); }

    @GET
    @Path("/{username}")
    @JsonView(View.Public.class)
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public List<Order> retrieveAll(@PathParam("username") String username) { return service.getAll(username); }

    @POST
    @Path("/fill/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @JsonView(View.Protected.class)
    @RolesAllowed("GUEST")
    @Produces(MediaType.TEXT_HTML)
    public void create(@PathParam("id") int order_id, List<OrderProduct> orderProducts) {
        service.fillOrder(orderProducts, order_id);
    }

    @POST
    @Path("/{username}")
    @Consumes(MediaType.APPLICATION_JSON)
    @JsonView(View.Protected.class)
    @RolesAllowed("GUEST")
    public int create(@PathParam("username") String username ) {
        return service.createOrder(username);
    }

    @PUT
    @Path("/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @JsonView(View.Protected.class)
    @RolesAllowed("ADMIN")
    public void update(@PathParam("id") int id ) {
        service.update();
    }

    @DELETE
    @Path("/{id}")
    @RolesAllowed("ADMIN")
    public void delete(@PathParam("id") int id)
    {
        service.delete();
    }


}
